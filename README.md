# COMP90051 Statistical Machine Learning - Project 1
## FaMaRa Group

Name                   | Student ID | Email                            | Kaggle username  | 
---------------------- | ---------- | -------------------------------- | ---------------- | 
1. Ratih Putri Pertiwi | 969864     | pertiwir@student.unimelb.edu.au  | ratihpp          |   
2. Fajri Fajri         | 1011213    | ffajri@student.unimelb.edu.au    | ffajri           |   
3. Martin Cheong       | 520942     | m.cheong3@student.unimelb.edu.au | cheongm          |   


This code is used for solving Project 1, link prediction, class Statistical Machine Learning (Semester 2) University of Melbourne. There are four main folders of this code
### 1. data_preprocessing
This folder comprises of:  
a. *get_overview.py*: for checking data summary.  
b. *generate_positive_edges.py*: for obtaining all list of edges from train.txt  
c. *generate_data.py*: for generating negative sample  
d. *generate_subset_data.py*: for creating subset of data randomly, resulting in 20%, 40%, 60%, 80%, and 100% data (shuffled)  
e. *generate_devset.py*: for creating train-set and dev-set from output of generate_subset_data.py  

### 2. dataset
All of dataset is placed in the folder. There are some folders: raw, heuristic, DeepWalk64, and DeepWalk128

### 3. extractor
This folder comprises of:  
a. *extract_deepwalk.py*: for obtaining edges representation with DeepWalk64 (Hadamard)  
b. *extract_deepwalk128.py*: for obtaining edges representation with DeepWalk128 (Hadamard)  
c. *heuristic.py* and *extract_heuristc.py*: for obtaining heuristic features such as: Common Neighbor, Jaccard etc.  
Note: The node embedding vector is obtained by running code from: https://github.com/phanein/deepwalk

### 4. training
This folder comprises of:  
a. *log_reg.py*: Implementation of logistic regression by using tensorflow.  
b. *MLP_5layers.py*: Implementation of Neural Network (5 layers).  
c. *cnn.py*: Implementation of CNN with 5 conv layers and 2 dense layer (for DeepWalk64).  
d. *cnn128.py*: Implementation of CNN with 5 conv layers and 2 dense layer (for DeepWalk128).  

Example of our training:   
`python cnn.py -ftr  ../dataset/deepwalk64/training90.txt -fte ../dataset/deepwalk64/testing.txt -fd ../dataset/deepwalk64/training10.txt -lr 0.002 -ep 100 -bs 100 -ds 1 -p`
