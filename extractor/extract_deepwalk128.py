import argparse
import numpy as np
# read deepwalk
deepwalk = {}

path='/home/fajri/Workspace/nodeembeds/deepwalk128.emb'

f = open(path, 'r')
for line in f.readlines()[1:]:
    data = line.strip().split(' ')
    node = data[0]
    vec = [float(i) for i in data[1:]]
    deepwalk[node] = vec

#hadamart
def extract_train():
    FILE_TRAIN = '../dataset/raw/training_raw_20.txt'
    FILE_OUTPUT = '../dataset/deepwalk128/training.txt'

    inp = open(FILE_TRAIN, 'r')
    outp = open(FILE_OUTPUT, 'w')
    c = 0
    for line in inp.readlines():
        data = line.strip().split('\t')
        label = data[2]
        node1 = data[0]
        node2 = data[1]
        vectors = np.multiply(deepwalk[node1], deepwalk[node2])
        str_vector = '\t'.join(str(i) for i in vectors)
        out = str_vector + '\t' +label+'\n'
        outp.write(out)

        c+=1
        if c%100000 == 0:
            print(str(c))
    outp.close()

#hadamart
def extract_test():
    FILE_TEST = '../dataset/raw/test-public.txt'
    FILE_OUTPUT = '../dataset/deepwalk128/testing.txt'

    inp = open(FILE_TEST, 'r')
    outp = open(FILE_OUTPUT, 'w')

    for line in inp.readlines()[1:]:
        data = line.strip().split('\t')
        ids = data[0]
        node1 = data[1]
        node2 = data[2]
        vectors = np.multiply(deepwalk[node1], deepwalk[node2])
        out = '\t'.join(str(i) for i in vectors) + '\t' +ids+'\n'
        outp.write(out)
    outp.close()

parser = argparse.ArgumentParser()
parser.add_argument("-tr", "--train", help="extract data train",
                    action="store_true")

parser.add_argument("-te", "--test", help="extract data test",
                    action="store_true")

args = parser.parse_args()
if args.train:
    print('Extract train')
    extract_train()
if args.test:
    print('Extract test')
    extract_test()


