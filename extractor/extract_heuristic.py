from heuristic import Heuristic
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-tr", "--train", help="extract data train",
                    action="store_true")

parser.add_argument("-te", "--test", help="extract data test",
                    action="store_true")

args = parser.parse_args()
if args.train:
    FILE_GRAPH = '../dataset/train.txt'
    FILE_TRAIN = '../dataset/training_raw_100.txt'
    FILE_OUTPUT = '../dataset/training_extracted_100.txt'
    h = Heuristic(FILE_GRAPH, FILE_TRAIN, FILE_OUTPUT)
    h.extract_train()
    
elif args.test:
    FILE_GRAPH = '../dataset/train.txt'
    FILE_TEST = '../dataset/test-public.txt'
    FILE_OUTPUT = '../dataset/test-public-extracted.txt'
    h = Heuristic(FILE_GRAPH, FILE_TEST, FILE_OUTPUT)
    h.extract_test()
