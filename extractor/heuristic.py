from networkx import nx
import math


class Heuristic(object):
    def __init__(self, file_graph, file_target, file_result):
        print('Initialization, reading the graph')
        fh = open (file_graph, 'rb')
        self.G = nx.read_adjlist(fh, create_using=nx.DiGraph(), delimiter='\t')
        print('Finish reading the graph')
        self.file_target = file_target
        self.file_result = file_result
    
    # Given two set
    # Return tupple (common_neighbors, jaccard, preferetial_attachment)
    def extract_set(self, set1, set2):
        common_neighbors = len(set1.intersection(set2))
        jaccard = 0
        if len(set1.union(set2)) != 0:
            jaccard =  len(set1.intersection(set2)) /float(len(set1.union(set2)))
        preferetial_attachment = len(set1) * len(set2)
        return common_neighbors, jaccard, preferetial_attachment

    # return 1 if node2 follow node1
    def is_followed(self, node1, node2):
        followed_by_node2 = set(self.G.successors(node2))
        if node1 in followed_by_node2:
            return 1
        return 0

    #in: predecessor
    #out: successor
    def extract_in_out_order1(self, node1, node2):
        in1 = set(self.G.predecessors(node1))
        in2 = set(self.G.predecessors(node2))
        cn_in, j_in, pa_in = self.extract_set(in1, in2)

        out1 = set(self.G.successors(node1))
        out2 = set(self.G.successors(node2))
        cn_out, j_out, pa_out = self.extract_set(out1, out2)
        
        return len(in1), len(in2), cn_in, j_in, pa_in, len(out1), len(out2), cn_out, j_out, pa_out

        
    def extract_train(self):
        f = open(self.file_target, 'r')
        w = open(self.file_result, 'w')
        count = 0
        for line in f.readlines():
            data = line.strip().split('\t')
            label = data[2]
            
            order1 = self.extract_in_out_order1(data[0], data[1])
            is_followed = self.is_followed(data[0],data[1]) 
            
            out = ''
            for data in order1:
                out += str(data)+'\t'
            out += str(is_followed)+'\t'
            out += label+'\n'
            w.write(out)
            if count%10000 == 0:
                print (str(count))
            count+=1
            
        w.close()

    def extract_test(self):
        f = open(self.file_target, 'r')
        w = open(self.file_result, 'w')
        for line in f.readlines()[1:]:
            data = line.strip().split('\t')
            ids = data[0]
            
            order1 = self.extract_in_out_order1(data[1], data[2])
            is_followed = self.is_followed(data[0],data[1]) 
            
            out = ''
            for data in order1:
                out += str(data)+'\t'
            out += str(is_followed)+'\t'
            out += ids+'\n'
            w.write(out)
            
        w.close()

