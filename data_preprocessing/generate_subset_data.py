import random

FILE_INPUT = '../dataset/training_raw.txt'
FILE_OUTPUT = '../dataset/training_raw_X.txt'


#receive percentage N
def generate_sample(pos_edges, neg_edges, N):
    print ('Generate '+ str(N)+'% of data..')
    pos_sample = random.sample(pos_edges, int(len(pos_edges)*N/100))
    neg_sample = random.sample(neg_edges, int(len(neg_edges)*N/100))
    sample = pos_sample + neg_sample
    random.shuffle(sample)
    #write to file
    filename = FILE_OUTPUT.replace('X', str(N))
    print ('Writing to file: '+ filename)
    f = open(filename, 'w')
    for edge in sample:
        f.write(edge+'\n')
    f.close()


print ('Scanning file from: '+ FILE_INPUT)
resource = open(FILE_INPUT, "r")
pos_edges = []
neg_edges = []
for line in resource.readlines():
    data = line.strip().split('\t')
    if data[2] == '1':
        pos_edges.append(line.strip())
    else:
        neg_edges.append(line.strip())

generate_sample(pos_edges, neg_edges, 20)
generate_sample(pos_edges, neg_edges, 40)
generate_sample(pos_edges, neg_edges, 60)
generate_sample(pos_edges, neg_edges, 80)
generate_sample(pos_edges, neg_edges, 100)
