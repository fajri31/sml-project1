import argparse
import numpy as np
from sklearn.model_selection import train_test_split

parser = argparse.ArgumentParser()
parser.add_argument("-i", "--input", help="provide extracted data", type=str)
args = parser.parse_args()
FILE = args.input

np_array = np.loadtxt(FILE, delimiter='\t')
label_idx = np_array.shape[1]-1
Y = np_array[:,[label_idx]]
X = np_array[:,[i for i in range(0,label_idx)]]

X_train, X_dev, y_train, y_dev = train_test_split(X, Y,
                                                    stratify=Y, 
                                                    test_size=0.1)

FILE_TRAIN = FILE.replace('.txt', '90.txt')
FILE_DEV = FILE.replace('.txt', '10.txt')

f = open(FILE_TRAIN, 'w')
for i in range(X_train.shape[0]):
    out = '\t'.join(str(data) for data in X_train[i]) + '\t' + str(y_train[i][0]) + '\n'
    f.write(out)
f.close()

f = open(FILE_DEV, 'w')
for i in range(X_dev.shape[0]):
    out = '\t'.join(str (data) for data in X_dev[i]) + '\t' + str(y_dev[i][0]) + '\n'
    f.write(out)
f.close()
