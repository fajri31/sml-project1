from collections import defaultdict

FILE_TRAIN = '../dataset/train.txt'
FILE_TEST = '../dataset/test-public.txt'

#Check Total of node and Vertices

print ('Checking File: '+ FILE_TRAIN)
resource = open(FILE_TRAIN, "r")
nodes = defaultdict(lambda:0) # count node frequency
edges = defaultdict(lambda:0) # count edge frequency (supposed to be 1 for all)
for line in resource.readlines():
    data = line.strip().split('\t')
    first_node = data[0]
    nodes[first_node] +=  1
    for node in data[1:]:
        nodes[node] += 1
        edges[first_node + ' ' + node] += 1
print ('Number of nodes: ' + str(len(nodes.keys())))
print ('Number of edges: ' + str(len(edges.keys())))

#Check Whether edges in testing set doesnt exist in training set

print ('Checking File: ' + FILE_TEST)
count = 0
resource = open(FILE_TEST, "r")
for line in resource.readlines()[1:]:
    data = line.strip().split('\t')
    if edges[data[0] + ' ' + data[1]] > 0:
        count += 1
print ('There is ' + str(count) + ' edge in testing set which also exist in training set')
