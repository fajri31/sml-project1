from collections import defaultdict
from random import randint

FILE_TRAIN = '../dataset/train.txt'
FILE_TEST = '../dataset/test-public.txt'
FILE_OUTPUT = '../dataset/training_raw.txt'

#Check Total of node and Vertices
print ('Scanning file from: '+ FILE_TRAIN)
resource = open(FILE_TRAIN, "r")
nodes = set()
positive_edges = defaultdict(lambda:0) # count edge frequency (supposed to be 1 for all)
for line in resource.readlines():
    data = line.strip().split('\t')
    nodes.add(data[0])
    for node in data[1:]:
        nodes.add(node)
        positive_edges[data[0] + '\t' + node] += 1

print ('Scanning file from: '+ FILE_TEST)
resource = open(FILE_TEST, "r")
edges_in_test = {}
for line in resource.readlines()[1:]:
    data = line.strip().split('\t')
    edges_in_test[data[1]+'\t'+data[2]] = 1

# Generate negative example by random sampling
print ('Generating training_raw.txt file..')
positive_example_count = len(positive_edges.keys())
nodes = list(nodes)

negative_edges = {}
while (positive_example_count > 0):
    index1 = randint(0, len(nodes)-1)
    index2 = randint(0, len(nodes)-1)
    key = str(nodes[index1]) + '\t' + str(nodes[index2])
    if (positive_edges.get(key, 0) == 0 and negative_edges.get(key, 0) == 0 and edges_in_test.get(key, 0) == 0):
        negative_edges[key] = 1
        positive_example_count -= 1

#write to file

print ('Writing to file: '+ FILE_OUTPUT)
f = open(FILE_OUTPUT, 'w')

for edge in positive_edges.keys():
    f.write(edge + '\t1\n')
for edge in negative_edges.keys():
    f.write(edge + '\t0\n')

f.close()


