import random

FILE_INPUT = '../dataset/training_raw.txt'

f = open('../dataset/positive_edges.txt', 'w')
print ('Scanning file from: '+ FILE_INPUT)
resource = open(FILE_INPUT, "r")
for line in resource.readlines():
    data = line.strip().split('\t')
    if data[2] == '1':
        f.write(data[0]+' '+data[1]+'\n')

f.close()
