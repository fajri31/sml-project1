from sklearn.model_selection import KFold
from sklearn.metrics import auc, accuracy_score, f1_score, mean_squared_error, roc_curve
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
import math
import time
import sys
import argparse
import numpy as np
import tflearn
import itertools
FILE_TRAIN = ''
FILE_TEST = ''
FILE_DEV = ''
GENERATE_PREDICTION=False
EARLYSTOPPING=False

# Parameters
learning_rate = 0.0002
training_epochs = 100
batch_size = 100
num_steps = 2000
display_step = 1
n_classes = 1

parser = argparse.ArgumentParser()
parser.add_argument("-ftr", "--filetrain", help="provide file input", type=str)
parser.add_argument("-fte", "--filetest", help="provide file test",type=str)
parser.add_argument("-fd", "--filedev", help="provide file dev",type=str)
parser.add_argument("-p", "--predict", help="make prediction from test (no k-fold)", action="store_true")
parser.add_argument("-r", "--regularizer", help="set regularizer L2", action="store_true")
parser.add_argument("-es", "--earlystopping", help="set early stopping true", action="store_true")
parser.add_argument("-lr", "--learningrate", help="set learning rate (default 0.0002", type=float)
parser.add_argument("-ep", "--epoch", help="set epoch (default 100)", type=int)
parser.add_argument("-bs", "--batchsize", help="set batch size (default 100)", type=int)
parser.add_argument("-ds", "--displaystep", help="set display step (default 1)", type=int)
parser.add_argument("-ns", "--noscaler", help="set not to use scaler (by default it always uses scaler)", action="store_true")

args = parser.parse_args()
if args.filetrain:
    print('Will read file train from '+args.filetrain)
    FILE_TRAIN=args.filetrain
else:
    print('Please provide file train!')
if args.filetest:
    print('Will read file test from '+args.filetest)
    FILE_TEST=args.filetest
else:
    print('please provide file test!')
if args.filedev:
    print('Will read file dev from '+args.filedev)
    FILE_DEV=args.filedev
else:
    print('please provide file dev!')
if args.predict:
    GENERATE_PREDICTION=True
if args.earlystopping:
    EARLYSTOPPING=True
if args.learningrate:
    learning_rate=args.learningrate
    print ('Using learning rate: '+ str(learning_rate))
if args.epoch:
    training_epochs=args.epoch
    print ('Using epoch: '+ str(training_epochs))
if args.batchsize:
    batch_size=args.batchsize
    print ('Using batch size: '+ str(batch_size))
if args.displaystep:
    display_step=args.displaystep
    print ('Using display step: '+ str(display_step))

#scaler
scaler = StandardScaler()

#read train file
np_array = np.loadtxt(FILE_TRAIN, delimiter='\t')
label_idx = np_array.shape[1]-1
Y = np_array[:,[label_idx]]
X = np_array[:,[i for i in range(0,label_idx)]]
if not args.noscaler:
    X = scaler.fit_transform(X)

#read dev file
np_array = np.loadtxt(FILE_DEV, delimiter='\t')
label_idx = np_array.shape[1]-1
Y_dev = np_array[:,[label_idx]]
X_dev = np_array[:,[i for i in range(0,label_idx)]]
if not args.noscaler:
    X_dev = scaler.transform(X_dev)

#read test file
np_array = np.loadtxt(FILE_TEST, delimiter='\t')
id_idx = np_array.shape[1]-1
ids = np_array[:,[id_idx]]
X_test = np_array[:,[i for i in range(0,id_idx)]]
if not args.noscaler:
    X_test = scaler.transform(X_test)
print ('Finish reading and normalizing data')

# Beta for L2 regularization
dropout = 0.01

def cnn_model_fn(features, labels, mode):
    """Model function for CNN."""
    # Input Layer
    input_layer = tf.reshape(features["data"], [-1, 64, 1])
    
    # Convolutional Layer #1
    conv1 = tf.layers.conv1d(
        inputs=input_layer,
        filters=4,
        kernel_size=9,
        padding="same",
        activation=tf.nn.tanh)
    
    # Pooling Layer #1
    pool1 = tf.layers.max_pooling1d(inputs=conv1, pool_size=2, strides=2)
    
    # Convolutional Layer #2 and Pooling Layer #2
    conv2 = tf.layers.conv1d(
        inputs=pool1,
        filters=8,
        kernel_size=5,
        activation=tf.nn.tanh)
    pool2 = tf.layers.max_pooling1d(inputs=conv2, pool_size=2, strides=2)
   
    # Convolutional Layer #3 and Pooling Layer #2
    conv3 = tf.layers.conv1d(
        inputs=pool2,
        filters=16,
        kernel_size=5,
        activation=tf.nn.tanh)
    pool3 = tf.layers.max_pooling1d(inputs=conv3, pool_size=2, strides=2)


    # Dense Layer
    pool3_flat = tf.reshape(pool3, [-1, 5 * 16])
    dense = tf.layers.dense(inputs=pool3_flat, units=40, activation=tf.nn.tanh)
    dropout = tf.layers.dropout(
        inputs=dense, rate=0.01, training=mode == tf.estimator.ModeKeys.TRAIN)
    
    # Logits Layer
    logits = tf.layers.dense(inputs=dropout, units=1)
    
    predictions = {
        # Generate predictions (for PREDICT and EVAL mode)
        "classes": (tf.nn.sigmoid(logits) >= 0.5),
        # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
        # `logging_hook`.
        "probabilities": tf.nn.sigmoid(logits, name="sigmoid_tensor")
    }
    
    if mode == tf.estimator.ModeKeys.PREDICT:
      return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)
    
    # Calculate Loss (for both TRAIN and EVAL modes)
    loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=labels, logits=logits))
    
    # Configure the Training Op (for TRAIN mode)
    if mode == tf.estimator.ModeKeys.TRAIN:
      optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
      train_op = optimizer.minimize(
          loss=loss,
          global_step=tf.train.get_global_step())
      return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    
    # Add evaluation metrics (for EVAL mode)
    eval_metric_ops = {
        "accuracy": tf.metrics.accuracy(
            labels=labels, predictions=predictions["classes"])}
    return tf.estimator.EstimatorSpec(
        mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)




print('Start doing Convolution')
# Build the Estimator
model = tf.estimator.Estimator(cnn_model_fn)

# Define the input function for training
input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'data': X}, y=Y,
    batch_size=batch_size, num_epochs=None, shuffle=True)
# Train the Model
model.train(input_fn, steps=num_steps)

# Evaluate the Model
# Define the input function for evaluating
input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'data': X_dev}, y=Y_dev,
    batch_size=batch_size, shuffle=False)
# Use the Estimator 'evaluate' method
e = model.evaluate(input_fn)

print("Testing Accuracy:", e['accuracy'])

input_fn = tf.estimator.inputs.numpy_input_fn(
        x={'data':X_test}, shuffle=False)

if GENERATE_PREDICTION:
    predictions = model.predict(input_fn)
    y_pred = []
    filename = '../dataset/result-'+str(time.strftime('%d-%m-%Y_%H:%M:%S'))+'.csv'
    f = open(filename, 'w')
    print('Writing to '+ filename)
    f.write('Id,Prediction\n')
    for pred in predictions:
        y_pred.append(pred['probabilities'][0])

    for i in range(len(y_pred)):
        f.write(str(int(ids[i][0]))+','+str(y_pred[i])+'\n')
    f.close()
    print (sum(y_pred))
