from sklearn.model_selection import KFold
from sklearn.metrics import auc, accuracy_score, f1_score, mean_squared_error, roc_curve
from sklearn.preprocessing import StandardScaler
import tensorflow as tf
import math
import time
import sys, argparse
import numpy as np


FILE_TRAIN = ''
FILE_TEST = ''
GENERATE_PREDICTION=False
EARLYSTOPPING=False

# Parameters
learning_rate = 0.002
training_epochs = 100
batch_size = 100
display_step = 1

parser = argparse.ArgumentParser()
parser.add_argument("-ftr", "--filetrain", help="provide file input", type=str)
parser.add_argument("-fte", "--filetest", help="provide file test",type=str)
parser.add_argument("-fd", "--filedev", help="provide file dev",type=str)
parser.add_argument("-p", "--predict", help="make prediction from test (no k-fold)", action="store_true")
parser.add_argument("-r", "--regularizer", help="set regularizer L2", action="store_true")
parser.add_argument("-es", "--earlystopping", help="set early stopping true", action="store_true")
parser.add_argument("-lr", "--learningrate", help="set learning rate (default 0.002", type=float)
parser.add_argument("-ep", "--epoch", help="set epoch (default 100)", type=int)
parser.add_argument("-bs", "--batchsize", help="set batch size (default 100)", type=int)
parser.add_argument("-ds", "--displaystep", help="set display step (default 1)", type=int)
parser.add_argument("-ns", "--noscaler", help="set not to use scaler (by default it always uses scaler)", action="store_true")

args = parser.parse_args()
if args.filetrain:
    print('Will read file train from '+args.filetrain)
    FILE_TRAIN=args.filetrain
else:
    print('Please provide file train!')
if args.filetest:
    print('Will read file test from '+args.filetest)
    FILE_TEST=args.filetest
else:
    print('please provide file test!')
if args.filedev:
    print('Will read file dev from '+args.filedev)
    FILE_DEV=args.filedev
else:
    print('please provide file dev!')
if args.predict:
    GENERATE_PREDICTION=True
if args.earlystopping:
    EARLYSTOPPING=True
if args.learningrate:
    learning_rate=args.learningrate
    print ('Using learning rate: '+ str(learning_rate))
if args.epoch:
    training_epochs=args.epoch
    print ('Using epoch: '+ str(training_epochs))
if args.batchsize:
    batch_size=args.batchsize
    print ('Using batch size: '+ str(batch_size))
if args.displaystep:
    display_step=args.displaystep
    print ('Using dsiplay step: '+ str(display_step))

#scaler
scaler = StandardScaler()

#read train file
np_array = np.loadtxt(FILE_TRAIN, delimiter='\t')
label_idx = np_array.shape[1]-1
Y = np_array[:,[label_idx]]
X = np_array[:,[i for i in range(0,label_idx)]]
if not args.noscaler:
    X = scaler.fit_transform(X)

#read dev file
np_array = np.loadtxt(FILE_DEV, delimiter='\t')
label_idx = np_array.shape[1]-1
Y_dev = np_array[:,[label_idx]]
X_dev = np_array[:,[i for i in range(0,label_idx)]]
if not args.noscaler:
    X_dev = scaler.transform(X_dev)

#read test file
np_array = np.loadtxt(FILE_TEST, delimiter='\t')
id_idx = np_array.shape[1]-1
ids = np_array[:,[id_idx]]
X_test = np_array[:,[i for i in range(0,id_idx)]]
if not args.noscaler:
    X_test = scaler.transform(X_test)
print ('Finish reading and normalizing data')

# tf Graph Input
x = tf.placeholder(tf.float32, [None, X.shape[1]]) # 2 feature
y = tf.placeholder(tf.float32, [None, 1]) # 1 probability
prediction = tf.placeholder(tf.float32, [None, 1]) # 1 probability

# Set model weights
W = tf.Variable(tf.zeros([X.shape[1], 1]))
b = tf.Variable(tf.zeros([1]))

# Construct model
pred = tf.nn.sigmoid(tf.matmul(x, W) + b) # Sigmoid

# Minimize error using cross entropy
# cost = tf.reduce_mean(-tf.reduce_sum(y*tf.log(pred), reduction_indices=1))
cost = tf.reduce_sum(tf.pow(pred-y, 2))/(2*batch_size)

# Gradient Descent
optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)

# Initialize the variables (i.e. assign their default value)
init = tf.global_variables_initializer()

def batches(batch_size, features, labels):
    X_batch = []
    y_batch = []

    sample_size = len(features)
    for start_i in range(0, sample_size, batch_size):
        end_i = start_i + batch_size
        batchX = features[start_i:end_i]
        batchy = labels[start_i:end_i]
        X_batch.append(batchX)
        y_batch.append(batchy)
    return X_batch, y_batch

#return rmse, auc, accuracy, f1-measure
#y_pred in probability
def evaluation(y, y_pred):
    rmse = math.sqrt(mean_squared_error(y, y_pred))
    y = (y == 1)
    y_pred = (y_pred >= 0.5)
    acc = accuracy_score(y, y_pred)
    f1 = f1_score(y, y_pred)
    fpr, tpr, thresholds = roc_curve(y, y_pred)
    AUC = auc(fpr, tpr)
    return AUC, acc, f1, rmse

def run_train(session):
    session.run(init)
    X_batch, Y_batch = batches(batch_size, X, Y)
    # Run the initializer

    # Training cycle
    for epoch in range(training_epochs):
        avg_cost = 0.
        total_batch = int(X.shape[0]/batch_size)
        # Loop over all batches
        for i in range(total_batch):
            # Run optimization op (backprop) and cost op (to get loss value)
            _,c = session.run([optimizer, cost], feed_dict={x: X_batch[i], y: Y_batch[i]})
            # Compute average loss
            avg_cost += c
        avg_cost /= total_batch
        # Display logs per epoch step
        if (epoch+1) % display_step == 0:
            print("Epoch:", '%04d' % (epoch+1), "cost=", "{:.9f}".format(avg_cost))
    print("Optimization Finished!")
    print("Now evaluating dev set..")
    y_pred = pred.eval({x: X_dev})
    r = evaluation(Y_dev, y_pred)
    return r

def generate_result(sess):
    print('Generate Prediction for test set..')
    Y_pred = pred.eval({x: X_test})

    filename = '../dataset/result-'+str(time.strftime('%d-%m-%Y %H:%M:%S'))+'.csv'
    f = open(filename, 'w')
    f.write('Id,Prediction\n')
    for i in range(len(Y_pred)):
        f.write(str(int(ids[i][0]))+','+str(Y_pred[i][0])+'\n')
    f.close()
    print (sum(Y_pred))

with tf.Session(config=tf.ConfigProto(allow_soft_placement=True)) as sess:
#with tf.Session() as sess:
    result = run_train(sess)
    print ('Dev Set Result: ')
    print ('AUC : '+str(result[0]))
    print ('acc : '+str(result[1]))
    print ('f1  : '+str(result[2]))
    print ('rmse: '+str(result[3]))
    if GENERATE_PREDICTION:
        generate_result(sess)
